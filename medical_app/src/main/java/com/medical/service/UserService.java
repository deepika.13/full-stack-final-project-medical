package com.medical.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.medical.model.User;

public interface UserService {

	public User addUser(User usr);
	public User updateUser();
	public User deleteUser();
	public List<User> vaidateUser(User usr);

	
	
}
