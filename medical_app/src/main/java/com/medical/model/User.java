package com.medical.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import org.hibernate.annotations.ManyToAny;

@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int uid;
	
	private String name;
	private String password;
	private String emailId;
	private String address;
	private boolean enabled = false;
	
	/*
	 * @ManyToMany(cascade=CascadeType.ALL,fetch = FetchType.EAGER)
	 * 
	 * @JoinTable(name="user_roles",joinColumns = {@JoinColumn(name="uid")},
	 * inverseJoinColumns = {@JoinColumn(name="rid")}) private Set<Roles> roles =
	 * new HashSet<>();
	 */

	  @ManyToMany(fetch = FetchType.LAZY,
	            cascade = {
	                CascadeType.PERSIST,
	                CascadeType.MERGE
	            })
	    @JoinTable(name = "user_roles",
	            joinColumns = { @JoinColumn(name = "uid") },
	            inverseJoinColumns = { @JoinColumn(name = "rid") })
	    private Set<Roles> roles = new HashSet<>();
	
	
	public int getUid() {
		return uid;
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Set<Roles> getRoles() {
		return roles;
	}

	public void setRoles(Set<Roles> roles) {
		this.roles = roles;
	}

	public User(int uid, String name, String password, String emailId, String address, boolean enabled,
			Set<Roles> roles) {
		super();
		this.uid = uid;
		this.name = name;
		this.password = password;
		this.emailId = emailId;
		this.address = address;
		this.enabled = enabled;
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "User [uid=" + uid + ", name=" + name + ", password=" + password + ", emailId=" + emailId + ", address="
				+ address + ", enabled=" + enabled + ", roles=" + roles + "]";
	}
	
	
	public User() {
		
		
	}
	
	

}
