package com.medical.model;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Products {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int pid;
	
	private String name;
	private String description;
	private int price;
	private String imageType;
	
	public String getImageType() {
	return imageType;
}

public void setImageType(String imageType) {
	this.imageType = imageType;
}

	public Products(int pid, String name, String description, int price, String imageType, byte[] image) {
	super();
	this.pid = pid;
	this.name = name;
	this.description = description;
	this.price = price;
	this.imageType = imageType;
	this.image = image;
}

	@Column(name = "image", length = 1000)
	private byte[] image;

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public byte[] getImage() {
		
		//return image;()
		
		System.out.println("entered getter of image length"+decompressBytes(image).length);

		return decompressBytes(image);
		
		
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public Products(int pid, String name, String description, int price, byte[] image) {
		super();
		this.pid = pid;
		this.name = name;
		this.description = description;
		this.price = price;
		this.image = image;
	}

	public Products(String name, String imageType, byte[] image) {
		super();
		this.name = name;
		this.imageType = imageType;
		this.image = image;
	}	
	
	public Products() {
		super();
		
	}
	
	 public static byte[] decompressBytes(byte[] data) {

	        Inflater inflater = new Inflater();

	        inflater.setInput(data);

	        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

	        byte[] buffer = new byte[1024];

	        try {

	            while (!inflater.finished()) {

	                int count = inflater.inflate(buffer);

	                outputStream.write(buffer, 0, count);

	            }

	            outputStream.close();

	        } catch (IOException ioe) {

	        } catch (DataFormatException e) {

	        }

	        return outputStream.toByteArray();

	    }

}
