package com.medical.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Roles {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int rid;
	private String roleName;
	public int getRid() {
		return rid;
	}
	public void setRid(int rid) {
		this.rid = rid;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Roles(int rid, String roleName) {
		super();
		this.rid = rid;
		this.roleName = roleName;
	}
	@Override
	public String toString() {
		return "Roles [rid=" + rid + ", roleName=" + roleName + "]";
	}
	
	public Roles() {
		
		
	}
}
