package com.medical.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Orders {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int oid;
	private String name;
	private int price;
	private int qty;
	public int getOid() {
		return oid;
	}
	public void setOid(int oid) {
		this.oid = oid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public Orders(int oid, String name, int price, int qty) {
		super();
		this.oid = oid;
		this.name = name;
		this.price = price;
		this.qty = qty;
	}
	@Override
	public String toString() {
		return "Orders [oid=" + oid + ", name=" + name + ", price=" + price + ", qty=" + qty + "]";
	}
	

public Orders() {
	
	
}	

}
