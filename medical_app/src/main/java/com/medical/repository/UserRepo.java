package com.medical.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.medical.model.User;

@Repository
@Transactional
public interface UserRepo extends JpaRepository<User, Long> {

	//@Modifying
//	@Query("SELECT name FROM User usr  WHERE usr.name =  ?1 and usr.password = ?2")
	// List<User> findBynameAndpassword( String name,String password);
	public List<User> findByname(String name);


	
}
