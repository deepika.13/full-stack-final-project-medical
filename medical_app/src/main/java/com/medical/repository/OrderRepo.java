package com.medical.repository;


import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.medical.model.Orders;
import com.medical.model.Products;

@Repository
@Transactional
public interface OrderRepo extends JpaRepository<Orders, Long> {

	@Modifying
	@Query("UPDATE Orders o SET o.qty = ?1 WHERE o.name = ?2")
	void updateQty(int qty,String name);
	
	@Query("SELECT name FROM Orders ord  WHERE ord.name =  ?1")
	String findbyname( String name);
	

	@Query("SELECT qty FROM Orders ord  WHERE ord.name =  ?1")
	int findbyqty( String name);

	
} 


