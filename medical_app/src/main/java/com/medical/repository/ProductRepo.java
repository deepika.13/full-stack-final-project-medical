package com.medical.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.medical.model.Products;

@Repository
public interface ProductRepo extends JpaRepository<Products, Long> {
	
	Optional<Products> findByName(String name);

}
