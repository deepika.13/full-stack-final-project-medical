package com.medical.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.medical.model.Products;
import com.medical.repository.ProductRepo;
import com.medical.service.ProductService;

@Repository
public class ProductImpl implements ProductService{

	@Autowired
	ProductRepo product;
	
	@Override
	public List<Products> getProducts() {
		// TODO Auto-generated method stub
		
		return product.findAll();
	}

}
