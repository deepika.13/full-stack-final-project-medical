package com.medical.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.medical.model.Orders;
import com.medical.serviceImpl.OrderImpl;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/")
public class OrderController {
	
	@Autowired
	OrderImpl order;

	@PostMapping("/order")
	public Orders addProductToCart(@RequestBody Orders ord) {

		System.out.println("Entered post of OrderController");

		System.out.println("order is "+ord);
		 
		 return order.addProduct(ord);
	}
	
	@GetMapping("/getCart")
	public List<Orders> getCartProducts() {

		System.out.println("Entered @GetMapping of OrderController");

		return order.getCartProducts();
	}
	

}
