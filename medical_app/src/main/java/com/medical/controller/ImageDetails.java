package com.medical.controller;


import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity.BodyBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.medical.model.Products;
import com.medical.model.User;
import com.medical.repository.ProductRepo;
import com.medical.serviceImpl.ProductImpl;
import com.medical.serviceImpl.UserImpl;

@RestController
/*@RequestMapping("/")
 * @RequestMapping(path = "image")

 * */

@RequestMapping("/imagee")
@CrossOrigin(origins = "http://localhost:4200")
public class ImageDetails  {

	@Autowired
	ProductRepo repo;

	@Autowired
	ProductImpl pro;

	@PostMapping("/uploadd")	 
	public BodyBuilder uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {

		System.out.println("Original Image Byte Size - " + file.getBytes().length);
		Products img = new Products(0, file.getOriginalFilename(), file.getContentType(), 0, compressBytes(file.getBytes()));

		repo.save(img);

		return (org.springframework.http.RequestEntity.BodyBuilder) ResponseEntity.status(HttpStatus.OK);

	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////

	// compress the image bytes before storing it in the database

	public static byte[] compressBytes(byte[] data) {

		Deflater deflater = new Deflater();

		deflater.setInput(data);

		deflater.finish();

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

		byte[] buffer = new byte[1024];

		while (!deflater.finished()) {

			int count = deflater.deflate(buffer);

			outputStream.write(buffer, 0, count);

		}

		try {

			outputStream.close();

		} catch (IOException e) {

		}

		System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);

		return outputStream.toByteArray();

	}


	// uncompress the image bytes before returning it to the angular application

	public static byte[] decompressBytes(byte[] data) {

		Inflater inflater = new Inflater();

		inflater.setInput(data);

		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);

		byte[] buffer = new byte[1024];

		try {

			while (!inflater.finished()) {

				int count = inflater.inflate(buffer);

				outputStream.write(buffer, 0, count);

			}

			outputStream.close();

		} catch (IOException ioe) {

		} catch (DataFormatException e) {

		}

		return outputStream.toByteArray();

	}

	
	 @GetMapping( "/get/{imageName}" )
	     public Products getImage(@PathVariable("imageName") String imageName) throws IOException {
		 System.out.println("entered");

	         final Optional<Products> retrievedImage = repo.findByName(imageName);
	         System.out.println("retrievedImage"+retrievedImage.toString());
	         Products img = new Products(0, retrievedImage.get().getName(), 
	                  imageName, 0, decompressBytes(retrievedImage.get().getImage()));
	         System.out.println("img"+img);

	         return img;
	     }

}
